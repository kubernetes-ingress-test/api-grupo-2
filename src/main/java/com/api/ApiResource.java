package com.api;

import jakarta.ws.rs.GET;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.core.MediaType;

@Path("/")
public class ApiResource {
    
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getBase(){
        Response res = new Response();
        res.setCode(200);
        res.setMessage("/");
        res.setAplicacion("Api Grupo 2");
        return res;
    }
    
    @Path("/primer-nivel")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getPrimerNivel(){
        Response res = new Response();
        res.setCode(200);
        res.setMessage("/primer-nivel");
        res.setAplicacion("Api Grupo 2");
        return res;
    }
    
    @Path("/primer-nivel/segundo-nivel")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getSegundoNivel(){
        Response res = new Response();
        res.setCode(200);
        res.setMessage("/primer-nivel/segundo-nivel");
        res.setAplicacion("Api Grupo 2");
        return res;
    }
    
    @Path("/primer-nivel/segundo-nivel/tercer-nivel")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getTercerNivel(){
        Response res = new Response();
        res.setCode(200);
        res.setMessage("/primer-nivel/segundo-nivel/tercer-nivel");
        res.setAplicacion("Api Grupo 2");
        return res;
    }
    
    @Path("/primer-nivel/segundo-nivel/tercer-nivel/cuarto-nivel")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getCuartoNivel(){
        Response res = new Response();
        res.setCode(200);
        res.setMessage("/primer-nivel/segundo-nivel/tercer-nivel/cuarto-nivel");
        res.setAplicacion("Api Grupo 2");
        return res;
    }
    
    @Path("/primer-nivel/segundo-nivel/tercer-nivel/cuarto-nivel/quinto-nivel")
    @GET
    @Produces(MediaType.APPLICATION_JSON)
    public Response getQuintoNivel(){
        Response res = new Response();
        res.setCode(200);
        res.setMessage("/primer-nivel/segundo-nivel/tercer-nivel/cuarto-nivel/quinto-nivel");
        res.setAplicacion("Api Grupo 2");
        return res;
    }
}
